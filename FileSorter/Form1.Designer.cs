﻿namespace FileSorter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxMain = new System.Windows.Forms.ListBox();
            this.buttonMoveAll = new System.Windows.Forms.Button();
            this.listBoxDestinations = new System.Windows.Forms.ListBox();
            this.groupBoxAll = new System.Windows.Forms.GroupBox();
            this.groupBoxDestinations = new System.Windows.Forms.GroupBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.labelStatus = new System.Windows.Forms.Label();
            this.groupBoxStatus = new System.Windows.Forms.GroupBox();
            this.groupBoxButtons = new System.Windows.Forms.GroupBox();
            this.checkBoxAutorename = new System.Windows.Forms.CheckBox();
            this.buttonResetKey = new System.Windows.Forms.Button();
            this.buttonSelectFolder = new System.Windows.Forms.Button();
            this.buttonPresetFill = new System.Windows.Forms.Button();
            this.buttonClearDestinations = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBoxAll.SuspendLayout();
            this.groupBoxDestinations.SuspendLayout();
            this.groupBoxStatus.SuspendLayout();
            this.groupBoxButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxMain
            // 
            this.listBoxMain.FormattingEnabled = true;
            this.listBoxMain.Location = new System.Drawing.Point(6, 19);
            this.listBoxMain.Name = "listBoxMain";
            this.listBoxMain.Size = new System.Drawing.Size(709, 472);
            this.listBoxMain.TabIndex = 0;
            this.listBoxMain.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainList_KeyDown);
            // 
            // buttonMoveAll
            // 
            this.buttonMoveAll.Location = new System.Drawing.Point(6, 135);
            this.buttonMoveAll.Name = "buttonMoveAll";
            this.buttonMoveAll.Size = new System.Drawing.Size(75, 23);
            this.buttonMoveAll.TabIndex = 5;
            this.buttonMoveAll.Text = "Apply";
            this.buttonMoveAll.UseVisualStyleBackColor = true;
            this.buttonMoveAll.Click += new System.EventHandler(this.buttonMoveAll_Click);
            // 
            // listBoxDestinations
            // 
            this.listBoxDestinations.FormattingEnabled = true;
            this.listBoxDestinations.Location = new System.Drawing.Point(6, 19);
            this.listBoxDestinations.Name = "listBoxDestinations";
            this.listBoxDestinations.Size = new System.Drawing.Size(420, 472);
            this.listBoxDestinations.TabIndex = 6;
            // 
            // groupBoxAll
            // 
            this.groupBoxAll.Controls.Add(this.listBoxMain);
            this.groupBoxAll.Location = new System.Drawing.Point(12, 12);
            this.groupBoxAll.Name = "groupBoxAll";
            this.groupBoxAll.Size = new System.Drawing.Size(721, 500);
            this.groupBoxAll.TabIndex = 8;
            this.groupBoxAll.TabStop = false;
            this.groupBoxAll.Text = "Files and folders";
            // 
            // groupBoxDestinations
            // 
            this.groupBoxDestinations.Controls.Add(this.listBoxDestinations);
            this.groupBoxDestinations.Location = new System.Drawing.Point(739, 12);
            this.groupBoxDestinations.Name = "groupBoxDestinations";
            this.groupBoxDestinations.Size = new System.Drawing.Size(432, 500);
            this.groupBoxDestinations.TabIndex = 9;
            this.groupBoxDestinations.TabStop = false;
            this.groupBoxDestinations.Text = "Destinations";
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(6, 16);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(35, 13);
            this.labelStatus.TabIndex = 10;
            this.labelStatus.Text = "label1";
            // 
            // groupBoxStatus
            // 
            this.groupBoxStatus.Controls.Add(this.labelStatus);
            this.groupBoxStatus.Location = new System.Drawing.Point(12, 512);
            this.groupBoxStatus.Name = "groupBoxStatus";
            this.groupBoxStatus.Size = new System.Drawing.Size(1159, 40);
            this.groupBoxStatus.TabIndex = 11;
            this.groupBoxStatus.TabStop = false;
            this.groupBoxStatus.Text = "History and status.";
            // 
            // groupBoxButtons
            // 
            this.groupBoxButtons.Controls.Add(this.checkBoxAutorename);
            this.groupBoxButtons.Controls.Add(this.buttonResetKey);
            this.groupBoxButtons.Controls.Add(this.buttonSelectFolder);
            this.groupBoxButtons.Controls.Add(this.buttonPresetFill);
            this.groupBoxButtons.Controls.Add(this.buttonClearDestinations);
            this.groupBoxButtons.Controls.Add(this.buttonMoveAll);
            this.groupBoxButtons.Location = new System.Drawing.Point(1177, 12);
            this.groupBoxButtons.Name = "groupBoxButtons";
            this.groupBoxButtons.Size = new System.Drawing.Size(90, 500);
            this.groupBoxButtons.TabIndex = 12;
            this.groupBoxButtons.TabStop = false;
            this.groupBoxButtons.Text = "LameButtons";
            // 
            // checkBoxAutorename
            // 
            this.checkBoxAutorename.AutoSize = true;
            this.checkBoxAutorename.Location = new System.Drawing.Point(6, 164);
            this.checkBoxAutorename.Name = "checkBoxAutorename";
            this.checkBoxAutorename.Size = new System.Drawing.Size(83, 17);
            this.checkBoxAutorename.TabIndex = 10;
            this.checkBoxAutorename.Text = "Autorename";
            this.checkBoxAutorename.UseVisualStyleBackColor = true;
            // 
            // buttonResetKey
            // 
            this.buttonResetKey.Location = new System.Drawing.Point(6, 48);
            this.buttonResetKey.Name = "buttonResetKey";
            this.buttonResetKey.Size = new System.Drawing.Size(75, 23);
            this.buttonResetKey.TabIndex = 9;
            this.buttonResetKey.Text = "ResetKey";
            this.buttonResetKey.UseVisualStyleBackColor = true;
            this.buttonResetKey.Click += new System.EventHandler(this.NewKeyPathButton_Click);
            // 
            // buttonSelectFolder
            // 
            this.buttonSelectFolder.Location = new System.Drawing.Point(6, 19);
            this.buttonSelectFolder.Name = "buttonSelectFolder";
            this.buttonSelectFolder.Size = new System.Drawing.Size(75, 23);
            this.buttonSelectFolder.TabIndex = 8;
            this.buttonSelectFolder.Text = "SelectFolder";
            this.buttonSelectFolder.UseVisualStyleBackColor = true;
            this.buttonSelectFolder.Click += new System.EventHandler(this.newFolderButton_Click);
            // 
            // buttonPresetFill
            // 
            this.buttonPresetFill.Location = new System.Drawing.Point(6, 77);
            this.buttonPresetFill.Name = "buttonPresetFill";
            this.buttonPresetFill.Size = new System.Drawing.Size(75, 23);
            this.buttonPresetFill.TabIndex = 7;
            this.buttonPresetFill.Text = "Presets";
            this.buttonPresetFill.UseVisualStyleBackColor = true;
            this.buttonPresetFill.Click += new System.EventHandler(this.DestinationsFileButton_Click);
            // 
            // buttonClearDestinations
            // 
            this.buttonClearDestinations.Location = new System.Drawing.Point(6, 106);
            this.buttonClearDestinations.Name = "buttonClearDestinations";
            this.buttonClearDestinations.Size = new System.Drawing.Size(75, 23);
            this.buttonClearDestinations.TabIndex = 6;
            this.buttonClearDestinations.Text = "Clear";
            this.buttonClearDestinations.UseVisualStyleBackColor = true;
            this.buttonClearDestinations.Click += new System.EventHandler(this.ClearDestinationsButton_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1304, 563);
            this.Controls.Add(this.groupBoxButtons);
            this.Controls.Add(this.groupBoxStatus);
            this.Controls.Add(this.groupBoxDestinations);
            this.Controls.Add(this.groupBoxAll);
            this.Name = "Form1";
            this.Text = "File Sorter";
            this.groupBoxAll.ResumeLayout(false);
            this.groupBoxDestinations.ResumeLayout(false);
            this.groupBoxStatus.ResumeLayout(false);
            this.groupBoxStatus.PerformLayout();
            this.groupBoxButtons.ResumeLayout(false);
            this.groupBoxButtons.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxMain;
        private System.Windows.Forms.Button buttonMoveAll;
        private System.Windows.Forms.ListBox listBoxDestinations;
        private System.Windows.Forms.GroupBox groupBoxAll;
        private System.Windows.Forms.GroupBox groupBoxDestinations;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.GroupBox groupBoxStatus;
        private System.Windows.Forms.GroupBox groupBoxButtons;
        private System.Windows.Forms.Button buttonClearDestinations;
        private System.Windows.Forms.Button buttonPresetFill;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button buttonSelectFolder;
        private System.Windows.Forms.Button buttonResetKey;
        private System.Windows.Forms.CheckBox checkBoxAutorename;
    }
}

