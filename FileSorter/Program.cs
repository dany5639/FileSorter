﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileSorter
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            var fileOut = new FileInfo($"FileSorter.log");
            if (fileOut.Exists)
                fileOut.Delete();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
