﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileSorter
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Key is the short filename, value is the full original path.
        /// </summary>
        public Dictionary<string, string> unsortedItems = new Dictionary<string, string>();

        /// <summary>
        /// Key is the keyboard key, value is the destination folder.
        /// </summary>
        public Dictionary<string, string> destinations = new Dictionary<string, string>();
       
        /// <summary>
        /// Key is the short filename, value is the new folder path.
        /// </summary>
        public Dictionary<string, string> finalOperations = new Dictionary<string, string>();

        public Form1()
        {
            InitializeComponent();

            // Clear existing logs
            if (File.Exists("FileSorter.log"))
                File.Delete("FileSorter.log");

            labelStatus.Text = $"Debug: cleared out FileSorter.log.";

            checkBoxAutorename.Checked = true;
        }

        public static void csv(string output)
        {
            var fileOut = new FileInfo($"FileSorter.log");

            using (var csvStream = fileOut.OpenWrite())
            using (var csvWriter = new StreamWriter(csvStream))
            {
                csvStream.Position = csvStream.Length;
                csvWriter.WriteLine(output);
            }
        }

        private void MainList_KeyDown(object sender, KeyEventArgs e)
        {
            // Allow Up and Down arrow keys to execute normally. Otherwise catch the key to move the file to the folder assigned to that key.
            // Up and down selects different files and folders in the list
            if (e.KeyData == Keys.Down ||
                e.KeyData == Keys.Up)
                return;

            // If it's not up or down, surpress the action
            e.Handled = true;
            e.SuppressKeyPress = true;

            string file = "";

            // Used for the undo button
            switch (e.KeyData)
            {
                case Keys.Home: // Undo button
                    if (finalOperations.Count == 0)
                        goto labelExit;

                    listBoxMain.Items.Add(finalOperations.Last().Key);

                    labelStatus.Text = $"Restoring {finalOperations.Last().Key}";

                    finalOperations.Remove(finalOperations.Last().Key);

                    goto labelExit;
                default: 
                    if (listBoxMain.Items.Count == 0) // Exit if no items are left
                        break;

                    // For any other key, move the file/folder to the folder assigned to that key.
                    if (destinations.ContainsKey(e.KeyData.ToString().ToLower()))
                    {
                        // move file to folder
                        file = listBoxMain.SelectedItem.ToString();
                        var newPath = destinations[e.KeyData.ToString().ToLower()];

                        finalOperations.Add(file, newPath);
                        csv($"Key {e.KeyData.ToString().ToLower()}: Moving {file} to {newPath}");
                        labelStatus.Text = $"Set {file} to {newPath}";
                        break;
                    }
                    else
                    {
                        folderBrowserDialog1.ShowDialog();
                        csv($"ERROR: destinations list does not contain a folder for key: {e.KeyData.ToString().ToLower()}");

                        destinations.Add(e.KeyData.ToString().ToLower(), folderBrowserDialog1.SelectedPath);
                        csv($"New key: {destinations.Last().Key}; New folder: {destinations.Last().Value}");
                        labelStatus.Text = $"New key: {destinations.Last().Key}; New folder: {destinations.Last().Value}";
                        goto labelExit;
                    }
            }

            HideMovedItem();

            labelExit:
            RefreshDestinationList();
        }

        private void HideMovedItem()
        {
            // Used to remove the selected item and if it was moved
            var SelectedIndex = listBoxMain.SelectedIndex;

            // Proceed only if an item is selected
            if (SelectedIndex == -1)
                return;

            // Remove the selected item
            listBoxMain.Items.RemoveAt(listBoxMain.SelectedIndex);

            // Select the next item in list.
            if (SelectedIndex < listBoxMain.Items.Count)
            {
                listBoxMain.SetSelected(SelectedIndex, true);
            }
            else // If the next item in list is outside the list length, select the previous one.
            {
                // Don't select anything if the items count is 0
                if (listBoxMain.Items.Count > 0)
                {
                    listBoxMain.SetSelected(listBoxMain.Items.Count - 1, true);
                }
            }
        }

        private void RefreshDestinationList()
        {            
            // Clear out the destinations list
            int itemsCount = listBoxDestinations.Items.Count;

            for (int i = 0; i < itemsCount; i++)
                listBoxDestinations.Items.RemoveAt(0);

            // Add all items
            foreach (var destination in destinations)
            {
                var newDestination = $"{destination.Key.ToUpper()} : {destination.Value}";

                if (!listBoxDestinations.Items.Contains(newDestination))
                {
                    listBoxDestinations.Items.Add(newDestination);
                }
                else
                {
                    listBoxDestinations.Items.Remove(newDestination);
                }
            }
        }
        
        private void buttonMoveAll_Click(object sender, EventArgs e)
        {
            csv("Pressed nuke button.");

            foreach (var file in finalOperations)
            {
                var oldFullFilePath = unsortedItems[file.Key];

                var newFilePath = Path.Combine(file.Value, file.Key);
                var newFolderPath = Path.Combine(file.Value, file.Key);

                csv($"Moving {oldFullFilePath} to {newFilePath}");
                labelStatus.Text = $"Moving {oldFullFilePath} to {newFilePath}";

                if (!Directory.Exists(file.Value))
                {
                    csv($"Creating new folder: {file.Value}");
                    labelStatus.Text = $"Creating new folder: {file.Value}";
                    Directory.CreateDirectory(file.Value);
                }

                if (File.Exists(oldFullFilePath))
                {
                    if (!File.Exists(newFilePath))
                        File.Move(oldFullFilePath, newFilePath);
                    else
                    {
                        csv($"ERROR: file already exists {newFilePath}");

                        if (!checkBoxAutorename.Checked)
                            goto labelExit;

                        int i = -1;
                        if (File.Exists(newFilePath))
                        {
                            var fileTime = File.GetLastWriteTime(oldFullFilePath.ToString());

                            var createddate = Convert.ToDateTime(fileTime).ToString("ddMMyyyy").ToString();

                            var noExtension = Path.GetFileNameWithoutExtension(newFilePath);
                            var extension = Path.GetExtension(newFilePath);

                            newFilePath = $"{file.Value}\\{noExtension}_{createddate}{extension}";

                            while (File.Exists(newFilePath))
                            {
                                i++;
                                newFilePath = $"{file.Value}\\{noExtension}_{createddate}_{i}{extension}";
                            }

                            File.Move(oldFullFilePath, newFilePath);
                        }
                    }
                }
                else if (Directory.Exists(oldFullFilePath))
                {
                    var fileTime = File.GetLastWriteTime(oldFullFilePath.ToString());
                    var createddate = Convert.ToDateTime(fileTime).ToString("ddMMyyyy").ToString();

                    if (!Directory.Exists(newFolderPath))
                    {
                        try
                        {
                            Directory.Move(oldFullFilePath, newFolderPath);
                        }
                        catch (System.IO.IOException err)
                        {
                            csv($"ERROR: cannot find or move {oldFullFilePath}: {err.Message}");
                            labelStatus.Text = $"ERROR: {err.Message}";
                        }
                    }
                    else
                    {
                        if (!checkBoxAutorename.Checked)
                            goto labelExit;

                        var newFolderName1 = $"{newFolderPath}_{createddate}";

                        int i = -1;
                        if (Directory.Exists(newFolderName1))
                        {
                            var newFolderName2 = $"{newFolderPath}_{createddate}";
                            newFolderName1 = newFolderName2;

                            while (Directory.Exists(newFolderName1))
                            {
                                i++;
                                newFolderName1 = $"{newFolderName2}_{i}";
                            }

                            try
                            {
                                Directory.Move(oldFullFilePath, newFolderName1);
                            }
                            catch (System.IO.IOException err)
                            {
                                csv($"ERROR: cannot find or move {oldFullFilePath}: {err.Message}");
                                labelStatus.Text = $"ERROR: {err.Message}";
                            }
                        }
                        else
                        {
                            try
                            {
                                Directory.Move(oldFullFilePath, newFolderName1);
                            }
                            catch (System.IO.IOException err)
                            {
                                csv($"ERROR: cannot find or move {oldFullFilePath}: {err.Message}");
                                labelStatus.Text = $"ERROR: {err.Message}";
                            }
                        }
                    }
                }
                else
                {
                    csv($"ERROR: cannot find or move {oldFullFilePath}");
                    labelStatus.Text = $"ERROR: cannot find or move {oldFullFilePath}";
                    goto labelExit;
                }

                // ToDo: deal with files and folders bearing the same name
            }

            labelExit:

            finalOperations = new Dictionary<string, string>();
        }
        
        private void PopulateDestinationsFromFile(FileInfo file)
        {
            using (var csvStream = file.OpenRead())
            using (var csvReader = new StreamReader(csvStream))
            {
                string line = "";

                while (line != null)
                {
                    line = csvReader.ReadLine();

                    if (line == null)
                        break;

                    var items = line.Split(",".ToCharArray());

                    var key = items[0].ToLower();
                    var des = items[1];

                    if (destinations.ContainsKey(key))
                        destinations[key] = des;
                    else
                        destinations.Add(key, des);
                }
            }

            RefreshDestinationList();
            labelStatus.Text = $"Imported destinations from {file.FullName}.";
        }

        private void ClearDestinationsButton_Click(object sender, EventArgs e)
        {
            destinations = new Dictionary<string, string>();
            RefreshDestinationList();
            labelStatus.Text = "Cleared destinations list.";
        }

        private void DestinationsFileButton_Click(object sender, EventArgs e)
        {
            //  folderBrowserDialog1.RootFolder = Environment.SpecialFolder.MyDocuments;
            openFileDialog1.InitialDirectory = Environment.CurrentDirectory;
            openFileDialog1.ShowDialog();

            // Get a prebuilt list of keys and destinations
            var folderDestinationPresetsFile = new FileInfo(openFileDialog1.FileName);
            if (folderDestinationPresetsFile.Exists)
            { 
                PopulateDestinationsFromFile(folderDestinationPresetsFile);

                labelStatus.Text = $"Added destinations from \"{openFileDialog1.FileName}\"";
            }
        }

        private void newFolderButton_Click(object sender, EventArgs e)
        {
            // Show a dialog for starting folder
            folderBrowserDialog1.SelectedPath = Environment.CurrentDirectory;
            folderBrowserDialog1.ShowDialog();

            if (folderBrowserDialog1.SelectedPath == null)
                return;

            // Enumerate all files. Should get from argument, provided folder
            // var files = Directory.EnumerateFiles(Directory.GetCurrentDirectory());
            var files = Directory.EnumerateFiles(folderBrowserDialog1.SelectedPath);

            // Enumerate all folders
            var directories = Directory.EnumerateDirectories(folderBrowserDialog1.SelectedPath);

            listBoxMain.Items.Clear();

            unsortedItems.Clear();

            // listBox1 is the main files and folder list
            foreach (var filename in files)
            {
                var filenameShort = filename.Split("\\".ToCharArray()).Last();

                listBoxMain.Items.Add(filenameShort);
                csv($"Added to listBox1: {filenameShort}");

                unsortedItems.Add(filenameShort, filename);
            }

            foreach (var dirName in directories)
            {
                var dirNameShort = dirName.Split("\\".ToCharArray()).Last();

                listBoxMain.Items.Add(dirNameShort);
                csv($"Added to listBox1: {dirNameShort}");

                unsortedItems.Add(dirNameShort, dirName);
            }

            RefreshDestinationList();
            labelStatus.Text = "Refreshed destinations list.";
        }

        private void NewKeyPathButton_Click(object sender, EventArgs e)
        {
            var text = listBoxDestinations.SelectedItem;
            if (text == null)
                return;

            var key = text.ToString()[0].ToString().ToLower();

            // Show a dialog for starting folder
            folderBrowserDialog1.SelectedPath = Environment.CurrentDirectory;
            folderBrowserDialog1.ShowDialog();

            if (folderBrowserDialog1.SelectedPath == null)
                return;

            if (destinations.ContainsKey(key))
            {
                destinations[key] = folderBrowserDialog1.SelectedPath;
                csv($"New path for key {key}: {destinations[key]}");
                labelStatus.Text = $"New path for key {key}: {destinations[key]}";
            }

            RefreshDestinationList();
        }

    }
}
